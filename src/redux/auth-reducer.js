import bridge from '@vkontakte/vk-bridge';
import {authAPI} from "../api/api";

const SET_DATA_DB = 'SET_DATA_DB';
const SET_DATA_VK = 'SET_DATA_VK';
const UPDATE_DATA_USER = 'UPDATE_DATA_USER';
const SET_RECORDS_USER = 'SET_RECORDS_USER';
const SET_RATING = 'SET_RATING';

let initialState = {
  isReady: false,
  isReadyDB: false
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA_VK:
      return {
        ...action.data,
        isReadyVK: true
      };
    case SET_DATA_DB:
      return {
        ...state,
        info: action.data.info,
        characteristics: action.data.characteristics,
        characteristicsWeekAgo: action.data.characteristicsWeekAgo,
        absolute: action.data.absolute,
        isReadyDB: true
      };
    case SET_RECORDS_USER:
      return {
        ...state,
        records: action.records
      };
    case SET_RATING:
      return {
        ...state,
        rating: action.rating
      };
    case UPDATE_DATA_USER:
      return {
        ...state,
        isReadyDB: false
      };
    default:
      return state;
  }
};

export const setDataUserVK = (data) => async dispatch => {
  dispatch(setDataUserVK_action(data));
};

export const getDataUser = (id) => async dispatch => {
  let response = await authAPI.me(id);
  dispatch(setDataUserDB_action({...response.data}));
};

export const updateDataUser = (id, settings) => async dispatch => {
  await authAPI.setSettings(id, settings);
  dispatch(updateDataUserDB_action());
  dispatch(getDataUser(id));
};

export const setNewCharacteristics = (auth, id_char_line, characteristics) => async dispatch => {
  await authAPI.setCharacteristics(auth.info.id, id_char_line, characteristics);
  dispatch(updateDataUserDB_action());
  dispatch(getDataUser(auth.id));
};

export const getRecordsUser = (id) => async dispatch => {
  let response = await authAPI.records(id);
  dispatch(setRecordsUser_action(response));
};

export const getRating = (id, field) => async dispatch => {
  let response = await authAPI.rating(id, field);
  let ids = [];
  response.items.forEach((user) => {
    ids.push(user.uid)
  });
  let ids_str = ids.join(",");
  bridge.send("VKWebAppCallAPIMethod",
    {
      "method": "users.get",
      "params":
        {
          "user_ids": ids_str,
          "fields": "photo_50",
          "v": "5.126",
          "access_token": "f570b0fff570b0fff570b0ffd8f504ddbfff570f570b0ffaafa18bb2725f76b82c8b72d"
        }
    }
  );

  bridge.subscribe(({detail: {type, data}}) => {
    if (type === "VKWebAppCallAPIMethodResult") {
      for (var i = 0; i < data.response.length; i++) {
        for (var j = 0; j < response.items.length; j++) {
          if (data.response[i].id === response.items[j].uid) {
            response.items[j] = Object.assign(response.items[j], data.response[i]);
            response.items[j]['field'] = field;
            break;
          }
        }
      }
      dispatch(setRating_action(response));
    }
  });
};

export const setDataUserVK_action = (data) => (
  {
    type: SET_DATA_VK,
    data
  });

export const setDataUserDB_action = (data) => (
  {
    type: SET_DATA_DB,
    data
  });

export const updateDataUserDB_action = () => (
  {
    type: UPDATE_DATA_USER
  });

export const setRecordsUser_action = (records) => (
  {
    type: SET_RECORDS_USER,
    records
  });

export const setRating_action = (rating) => (
  {
    type: SET_RATING,
    rating
  });

export default authReducer;