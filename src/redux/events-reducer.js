import {eventsAPI} from "../api/api";
import moment from "moment";

const SET_EVENTS = 'SET_EVENTS';

let currentDate = moment().format();
let currentDayDate = moment().format('YYYY MM DD');

let initialState = {
  events: null,
  firstDate: null,
  lastDate: null,
  numberCurrent: 0
};

const eventsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_EVENTS:
      let firstDate = action.data.data.length > 0 ? moment(action.data.data[0]).format() : currentDate;
      let lastDate = action.data.data.length > 0 ? moment(action.data.data[action.data.data.length - 1].date_event).format() : currentDate;
      let events = action.data.data;
      let eventsWithDates = getEventsWithDates(events, firstDate, lastDate);

      return {
        ...state,
        events: eventsWithDates,
        numberCurrent: getNumberCurrent(currentDayDate, eventsWithDates),
        firstDate: firstDate,
        lastDate: lastDate
      };
    default:
      return state;
  }
};

export const addEvent = (data) => async () => {
  let {id, date_event, time_event, range_time, fact_range_time, title, type, value_event, comments} = data;
  await eventsAPI.newEvent(id, date_event, time_event, range_time, fact_range_time, title, type, value_event, comments);
};

export const changeEvent = (data) => async () => {
  let {id, id_event, date_event, time_event, range_time, fact_range_time, title, type, value_event, comments} = data;
  await eventsAPI.changeEvent(id, id_event, date_event, time_event, range_time, fact_range_time, title, type, value_event, comments);
};

export const deleteEvent = (data) => async () => {
  let {id, id_event} = data;
  await eventsAPI.deleteEvent(id, id_event);
};


export const getAllDays = (id_user) => async dispatch => {
  let response = await eventsAPI.getAllDays(id_user);
  dispatch(setEvents_action(response));
  return response.data;
};

export const setEvents_action = (data) => (
  {
    type: SET_EVENTS,
    data
  });

function getNumberCurrent(currentDayDate, events) {
  if (events.length === 0) {
    return 365
  }

  for(let i = 0; i < events.length; i++) {
    if (moment(events[i].date).format('YYYY MM DD') == currentDayDate) {
      return i;
    }
  }
}

function getEventsWithDates(events, firstDate, lastDate) {
  let firstDateNew = firstDate;
  let lastDateNew = lastDate;
  if (moment(firstDate).isAfter(moment())) {
    firstDateNew = moment().format();
  }
  if (moment(lastDate).isBefore(moment())) {
    lastDateNew = moment().format();
  }
  const prevDate = moment(firstDateNew).subtract(365, 'days');
  const nextDate = moment(lastDateNew).add(365, 'days');

  let start = new Date(prevDate);
  let end = new Date(nextDate);
  let dateArr = [];
  while (start < end) {
    dateArr.push(moment(start).format());
    let newDate = start.setDate(start.getDate() + 1);
    start = new Date(newDate);
  }

  let result = [];
  dateArr.forEach((date) => {
    result.push({date, event: null});
    if (events) {
      Object.keys(events).forEach((key) => {
        if (moment(date).format('YYYY-MM-DD') === events[key].date_event) {
          if (result[result.length - 1]['event']) {
            result.push({date, event: events[key]});
          } else {
            result[result.length - 1]['event'] = events[key];
          }
        }
      })
    }
  });
  return result;
}

export default eventsReducer;