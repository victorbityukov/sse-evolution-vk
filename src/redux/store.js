import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import authReducer from "./auth-reducer";
import {composeWithDevTools} from "redux-devtools-extension";
import eventsReducer from "./events-reducer";

let reducers = combineReducers({
  auth: authReducer,
  events: eventsReducer,
});

const store = createStore(reducers,  composeWithDevTools(applyMiddleware(thunkMiddleware)));
export default store;