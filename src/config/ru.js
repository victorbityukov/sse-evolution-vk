export const LANG_RU = {
  'sse': "SSE",
  'strength': "Сила",
  'speed': "Скорость",
  'endurance': "Выносливость",
  'run': 'Бег',
  'swimming': 'Плаванье',
  'bike': 'Велосипед',
  'power': 'Сила',
  'other': 'Прочее'
};