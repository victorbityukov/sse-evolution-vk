import React from 'react'

const ArrowDown = ({children}) => (<><span style={{color: "#ff0000"}}>▼</span><sup>{children}</sup></>);

export default ArrowDown;