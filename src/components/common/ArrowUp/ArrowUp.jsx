import React from 'react'

const ArrowUp = ({children}) => (<><span style={{color: "#7fff00"}}>▲</span><sup>{children}</sup></>);

export default ArrowUp;