import React, {useState, useEffect} from "react";
import View from "@vkontakte/vkui/dist/components/View/View";
import EditEvent from "../../panels/Journal/EditEvent/EditEvent";
import JournalInfinity from "../../panels/Journal/JournalInfinity";

const ROUTES = {
  CALENDAR: 'calendar',
  EDIT_EVENT: 'edit-event',
  ADD_EVENT: 'add-event'
};

const CalendarView = ({id}) => {
  const [activePanel, setActivePanel] = useState(ROUTES.CALENDAR);
  const [event, setEvent] = useState(null);

  const go = panel => {
    setActivePanel(panel)
  };

  const goEvent = (panel, event) => {
    setEvent(event);
    setActivePanel(panel);
  };

  return (
    <View
      id={id}
      activePanel={activePanel}>
      <JournalInfinity
        id={ROUTES.CALENDAR}
        go={go}
        goEvent={goEvent}
        routes={ROUTES}
      />
      <EditEvent
        id={ROUTES.ADD_EVENT}
        go={go}
        route={ROUTES.CALENDAR}
        event={null}
      />
      <EditEvent
        id={ROUTES.EDIT_EVENT}
        go={go}
        route={ROUTES.CALENDAR}
        event={event}
      />
    </View>
  );
};

export default CalendarView;