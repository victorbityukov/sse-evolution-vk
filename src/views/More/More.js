import React, {useState} from "react";
import View from "@vkontakte/vkui/dist/components/View/View";
import More from "../../panels/More/More";
import Settings from "../../panels/More/Settings/Settings";
import Faq from "../../panels/More/Faq/Faq";
import Reports from "../../panels/More/Reports/Reports";
import Notifications from "../../panels/More/Notifications/Notifications";
import Recommendations from "../../panels/More/Recommendations/Recommendations";

const ROUTES = {
  MORE: 'more',
  SETTINGS: 'settings',
  FAQ: 'faq',
  INTEGRATION: 'integration',
  FRIENDS: 'friends',
  REPORTS: 'reports',
  NOTIFICATIONS: 'notifications',
  RECOMMENDATIONS: 'recommendations'
};

const MoreView = ({id}) => {
  const [activePanel, setActivePanel] = useState(ROUTES.MORE);

  const go = panel => {
    setActivePanel(panel)
  };

  return (
    <View
      id={id}
      activePanel={activePanel}>
      <More
        id={ROUTES.MORE}
        routes={ROUTES}
        go={go}
      />
      <Notifications
        id={ROUTES.NOTIFICATIONS}
        route={ROUTES.MORE}
        go={go}
      />
      <Recommendations
        id={ROUTES.RECOMMENDATIONS}
        route={ROUTES.MORE}
        go={go}
      />
      <Settings
        id={ROUTES.SETTINGS}
        route={ROUTES.MORE}
        go={go}
      />
      <Faq
        id={ROUTES.FAQ}
        route={ROUTES.MORE}
        go={go}
      />
      <Reports
      id={ROUTES.REPORTS}
      route={ROUTES.MORE}
      go={go}
      />
    </View>
  );
};

export default MoreView;