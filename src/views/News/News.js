import React, {useState} from "react";
import View from "@vkontakte/vkui/dist/components/View/View";
import News from "../../panels/News/News";
import ModalRoot from "@vkontakte/vkui/dist/components/ModalRoot/ModalRoot";
import ModalPage from "@vkontakte/vkui/dist/components/ModalPage/ModalPage";
import ModalPageHeader from "@vkontakte/vkui/dist/components/ModalPageHeader/ModalPageHeader";
import FormLayout from "@vkontakte/vkui/dist/components/FormLayout/FormLayout";
import FormLayoutGroup from "@vkontakte/vkui/dist/components/FormLayoutGroup/FormLayoutGroup";
import Cell from "@vkontakte/vkui/dist/components/Cell/Cell";
import Switch from "@vkontakte/vkui/dist/components/Switch/Switch";
import Select from "@vkontakte/vkui/dist/components/Select/Select";
import Icon24Recent from '@vkontakte/icons/dist/24/recent';

const ROUTES = {
  NEWS: 'news',
};

const MODALS = {
  FILTERS: 'filters'
};

const NEWS_FILTERS = {
  ALL: 'all',
  INTERESTING: 'interesting'
};


const NewsView = ({id}) => {
  const [activePanel, setActivePanel] = useState(ROUTES.NEWS);
  const [activeModal, setActiveModal] = useState(null);
  const [newsFilter, setNewsFilter] = useState(NEWS_FILTERS.ALL);

  const modal = (
    <ModalRoot
      activeModal={activeModal}
      onClose={() => setActiveModal(null)}>
      <ModalPage
        id={MODALS.FILTERS}
        onClose={() => setActiveModal(null)}
        header={
          <ModalPageHeader>
            Фильтры
          </ModalPageHeader>}>
        <FormLayout>
          <Select defaultValue={newsFilter} top="Новости">
            <option
              value={NEWS_FILTERS.ALL}
              onChange={() => setNewsFilter(NEWS_FILTERS.ALL)}
            >Все
            </option>
            <option
              value={NEWS_FILTERS.INTERESTING}
              onChange={() => setNewsFilter(NEWS_FILTERS.INTERESTING)}
            >Интересные
            </option>
          </Select>
          <FormLayoutGroup>
            <Cell before={<Icon24Recent/>}
                  asideContent={<Switch/>}>
              За последние 24 часа
            </Cell>
          </FormLayoutGroup>
        </FormLayout>
      </ModalPage>
    </ModalRoot>
  );
  return (
    <View
      id={id}
      activePanel={activePanel}
      modal={modal}
    >
      <News
        id={ROUTES.NEWS}
        activeModal={MODALS.FILTERS}
        setActiveModal={setActiveModal}
      />
    </View>
  );
};

export default NewsView;