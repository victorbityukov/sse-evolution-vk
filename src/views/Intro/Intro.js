import React, {useState, useEffect} from "react";
import View from "@vkontakte/vkui/dist/components/View/View";
import Intro from "../../panels/Intro/Intro";

const ROUTES = {
  INTRO: 'intro',
};

const IntroView = ({id, fetchedUser, route, go}) => {
  const [activePanel, setActivePanel] = useState(ROUTES.INTRO);
  useEffect(()=>{
    setTimeout(()=>{
      go(route);
    }, 2000)
  }, []);
  return (
    <View
      id={id}
      activePanel={activePanel}
    >
      <Intro
        id={ROUTES.INTRO}
        fetchedUser={fetchedUser}
      />
    </View>
  );
};

export default IntroView;