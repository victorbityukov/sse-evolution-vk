import React, {useState} from "react";
import View from "@vkontakte/vkui/dist/components/View/View";
import Profile from "../../panels/Profile/Profile";
import ProfileEdit from "../../panels/Profile/ProfileEdit/ProfileEdit";
import Point from "../../panels/Profile/Point/Point";
import Records from "../../panels/Profile/Records/Records";
import Food from "../../panels/Profile/Food/Food";
import ModalRoot from "@vkontakte/vkui/dist/components/ModalRoot/ModalRoot";
import ModalPage from "@vkontakte/vkui/dist/components/ModalPage/ModalPage";
import PanelHeaderButton from "@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton";
import {ModalPageHeader, ANDROID, IOS, usePlatform} from '@vkontakte/vkui';
import Icon24Done from "@vkontakte/icons/dist/24/done";
import FoodParameters from "../../panels/Profile/FoodParameters/FoodParameters";


const ROUTES = {
  PROFILE: 'profile',
  PROFILE_EDIT: 'profile-edit',
  RECORDS: 'records',
  FOOD: 'food',
  FOOD_PARAMETERS: 'food-parameters',
  POINT: 'point'
};

const MODALS = {
  YOUTUBE: 'youtube'
};

const ProfileView = ({id, goStory, stories, popout, setPopout}) => {
  const [activePanel, setActivePanel] = useState(ROUTES.PROFILE);
  const [activeModal, setActiveModal] = useState(null);
  const [linkVideo, setLinkVideo] = useState(null);

  const go = panel => {
    setActivePanel(panel)
  };

  const modalBack = () => {
    setActiveModal(null);
  };

  const platform = usePlatform();

  const modal = (
    <ModalRoot
      isBack={false}
      activeModal={activeModal}
      onClose={() => setActiveModal(null)}>
      <ModalPage
        id={MODALS.YOUTUBE}
        onClose={modalBack}
        header={
          <ModalPageHeader
            right={(
              <>
                {platform === ANDROID && <PanelHeaderButton onClick={modalBack}><Icon24Done/></PanelHeaderButton>}
                {platform === IOS && <PanelHeaderButton onClick={modalBack}>Готово</PanelHeaderButton>}
              </>
            )}
          >
            Видео
          </ModalPageHeader>}>
        {!!linkVideo &&
        <iframe
          width="100%"
          src={linkVideo.replace("youtu.be", "youtube.com/embed")}
          frameBorder="0"
          allow="autoplay; encrypted-media" allowFullScreen
        />}
      </ModalPage>
    </ModalRoot>
  );

  return (
    <View id={id}
          activePanel={activePanel}
          popout={popout}
          modal={modal}
    >
      <Profile
        id={ROUTES.PROFILE}
        routes={ROUTES}
        go={go}
        setPopout={setPopout}
        goStory={goStory}
        stories={stories}
        modal={MODALS.YOUTUBE}
        setActiveModal={setActiveModal}
        setLinkVideo={setLinkVideo}
      />
      <Point
        id={ROUTES.POINT}
        route={ROUTES.PROFILE}
        go={go}
      />
      <ProfileEdit
        id={ROUTES.PROFILE_EDIT}
        route={ROUTES.PROFILE}
        go={go}
      />
      <Records
        id={ROUTES.RECORDS}
        route={ROUTES.PROFILE}
        go={go}
        setPopout={setPopout}
      />
      <Food
        id={ROUTES.FOOD}
        routes={ROUTES}
        go={go}
      />
      <FoodParameters
        id={ROUTES.FOOD_PARAMETERS}
        route={ROUTES.FOOD}
        go={go}
      />
    </View>
  );
};

export default ProfileView;