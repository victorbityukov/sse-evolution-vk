import React, {useState} from "react";
import View from "@vkontakte/vkui/dist/components/View/View";
import Rating from "../../panels/Rating/Rating";

const ROUTES = {
  RATING: 'rating',
};

const RatingView = ({id}) => {
  const [activePanel, setActivePanel] = useState(ROUTES.RATING);
  return (
    <View
      id={id}
          activePanel={activePanel}>
      <Rating
        id={ROUTES.RATING}
      />

    </View>
  );
};

export default RatingView;