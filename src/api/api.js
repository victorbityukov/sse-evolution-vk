import * as axios from "axios";

const instance = axios.create({
  // baseURL: 'http://localhost/sse_api/api/',
  baseURL: 'https://sse.readman.pro/api/',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    // 'Access-Control-Allow-Origin': "*"
  },
  // withCredentials: true
});


export const authAPI = {
  me(id) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('event', 'auth');
    return instance.post(`profile`, formData).then((response) => {
      return response.data;
    });
  },
  setSettings(id, settings) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('settings', JSON.stringify(settings));
    formData.append('event', 'setSettings');
    return instance.post(`profile`, formData).then((response) => {
      return response.data;
    });
  },
  setCharacteristics(id, id_char_line, characteristics) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('id_char_line', id_char_line);
    formData.append('characteristics', JSON.stringify(characteristics));
    formData.append('event', 'setCharacteristics');
    return instance.post(`profile`, formData).then((response) => {
      return response.data;
    });
  },
  records(id) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('event', 'getRecords');
    return instance.post(`profile`, formData).then((response) => {
      return response.data.data;
    });
  },
  rating(id, field) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('field', field);
    formData.append('event', 'getRating');
    return instance.post(`profile`, formData).then((response) => {
      return response.data.data;
    });
  }
};

export const eventsAPI = {
  newEvent(id, date_event, time_event, range_time, fact_range_time, title, type, value_event, comments) {
    let formData = new FormData();
    formData.append('uid', id);
    formData.append('type', type);
    formData.append('title', title);
    formData.append('date_event', date_event);
    formData.append('time_event', time_event);
    formData.append('range_time', range_time);
    formData.append('fact_range_time', fact_range_time);
    formData.append('value_event', value_event);
    formData.append('comments', comments);
    formData.append('event', 'new');
    return instance.post(`events`, formData).then((response) => {
      return response.data;
    });
  },
  changeEvent(id, id_event, date_event, time_event, range_time, fact_range_time, title, type, value_event, comments) {
    let formData = new FormData();
    formData.append('uid', id);
    formData.append('id_event', id_event);
    formData.append('type', type);
    formData.append('title', title);
    formData.append('date_event', date_event);
    formData.append('time_event', time_event);
    formData.append('range_time', range_time);
    formData.append('fact_range_time', fact_range_time);
    formData.append('value_event', value_event);
    formData.append('comments', comments);
    formData.append('event', 'change');
    return instance.post(`events`, formData).then((response) => {
      return response.data;
    });
  },
  deleteEvent(id, id_event) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('id_event', id_event);
    formData.append('event', 'delete');
    return instance.post(`events`, formData).then((response) => {
      return response.data;
    });
  },
  getAllDays(id_user) {
    let formData = new FormData();
    formData.append('uid', id_user);
    formData.append('event', 'getAll');
    return instance.post(`events`, formData).then((response) => {
      return response.data;
    });
  }
};

