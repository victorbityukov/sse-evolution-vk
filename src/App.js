import React, {useState, useEffect} from 'react';
import bridge from '@vkontakte/vk-bridge';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import Snackbar from "@vkontakte/vkui/dist/components/Snackbar/Snackbar";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import Tabbar from "@vkontakte/vkui/dist/components/Tabbar/Tabbar";
import Epic from "@vkontakte/vkui/dist/components/Epic/Epic";
import TabbarItem from "@vkontakte/vkui/dist/components/TabbarItem/TabbarItem";
import Icon28NewsfeedOutline from "@vkontakte/icons/dist/28/newsfeed_outline";
import Icon24Error from "@vkontakte/icons/dist/24/error";
import Icon28UserOutline from '@vkontakte/icons/dist/28/user_outline';
import Icon28MoreHorizontal from '@vkontakte/icons/dist/28/more_horizontal';
import Icon28FavoriteOutline from '@vkontakte/icons/dist/28/favorite_outline';
import Icon28BillheadOutline from '@vkontakte/icons/dist/28/billhead_outline';
import '@vkontakte/vkui/dist/vkui.css';
import NewsView from "./views/News/News";
import ProfileView from "./views/Profile/Profile";
import JournalView from "./views/Journal/Journal";
import RatingView from "./views/Rating/Rating";
import MoreView from "./views/More/More";
import {getDataUser, setDataUserVK} from "./redux/auth-reducer";
import {connect} from "react-redux";
import IntroView from "./views/Intro/Intro";

const ROUTES = {
  NEWS: 'news',
  JOURNAL: 'journal',
  PROFILE: 'profile',
  RATING: 'rating',
  MORE: 'more',
  INTRO: 'intro'
};

const STORAGE_KEYS = {
  STATE: 'state',
  STATUS: 'viewStatus',
};

const App = ({auth, setDataUserVK, getDataUser}) => {
  const [activeStory, setActiveStory] = useState(ROUTES.INTRO);
  const [fetchedUser, setUser] = useState(null);
  const [fetchedState, setFetchedState] = useState(null);
  const [snackbar, setSnackbar] = useState(null);
  const [popout, setPopout] = useState(<ScreenSpinner size='large'/>);

  useEffect(() => {
    if (fetchedUser) {
      setDataUserVK(fetchedUser);
      getDataUser(fetchedUser.id);
    }
  }, [fetchedUser]);

  useEffect(() => {
    bridge.subscribe(({detail: {type, data}}) => {
      if (type === 'VKWebAppUpdateConfig') {
        const schemeAttribute = document.createAttribute('scheme');
        schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
        document.body.attributes.setNamedItem(schemeAttribute);
      }
    });

    async function fetchData() {
      const user = await bridge.send('VKWebAppGetUserInfo');
      const sheetState = await bridge.send('VKWebAppStorageGet', {keys: [STORAGE_KEYS.STATE, STORAGE_KEYS.STATUS]});
      if (Array.isArray(sheetState.keys)) {
        const data = {};
        sheetState.keys.forEach(({key, value}) => {
          try {
            data[key] = value ? JSON.parse(value) : {};
            switch (key) {
              case STORAGE_KEYS.STATE:
                setFetchedState(data[STORAGE_KEYS.STATE]);
                break;
              default:
                break;
            }
          } catch (error) {
            setSnackbar(<Snackbar
                layout='vertical'
                onClose={() => setSnackbar(null)}
                before={<Avatar size={24} style={{backgroundColor: 'var(--dynamic_red)'}}>
                  <Icon24Error fill='#fff'
                               width={14}
                               height={14}/></Avatar>}
                duration={900}
              >
                Проблема с получением данных из Storage
              </Snackbar>
            );
            setFetchedState({});
          }
        });

      } else {
        setFetchedState({});
      }
      setUser(user);
      setPopout(null);
    }

    fetchData();
  }, []);

  const go = story => {
    setActiveStory(story);
  };

  return (
    <>{activeStory === ROUTES.INTRO ?
      <IntroView id={ROUTES.INTRO} fetchedUser={fetchedUser} go={go} route={ROUTES.PROFILE}/> :
      <Epic activeStory={activeStory} tabbar={
        <Tabbar>
          <TabbarItem
            onClick={() => go(ROUTES.NEWS)}
            selected={activeStory === ROUTES.NEWS}
            data-story={ROUTES.NEWS}
            text="Новости"
            label="12"
          ><Icon28NewsfeedOutline/></TabbarItem>
          <TabbarItem
            onClick={() => go(ROUTES.JOURNAL)}
            selected={activeStory === ROUTES.JOURNAL}
            data-story={ROUTES.JOURNAL}
            text="Дневник"
          ><Icon28BillheadOutline/></TabbarItem>
          <TabbarItem
            onClick={() => go(ROUTES.PROFILE)}
            selected={activeStory === ROUTES.PROFILE}
            data-story={ROUTES.PROFILE}
            text="Профиль"
          ><Icon28UserOutline/></TabbarItem>
          <TabbarItem
            onClick={() => go(ROUTES.RATING)}
            selected={activeStory === ROUTES.RATING}
            data-story={ROUTES.RATING}
            text="Рейтинг"
          ><Icon28FavoriteOutline/></TabbarItem>
          <TabbarItem
            onClick={() => go(ROUTES.MORE)}
            selected={activeStory === ROUTES.MORE}
            data-story={ROUTES.MORE}
            text="Больше"
          ><Icon28MoreHorizontal/>
          </TabbarItem>
        </Tabbar>
      }>
        <NewsView id={ROUTES.NEWS}/>
        <JournalView id={ROUTES.JOURNAL}/>
        <ProfileView id={ROUTES.PROFILE} goStory={go} stories={ROUTES} popout={popout} setPopout={setPopout}/>
        <RatingView id={ROUTES.RATING}/>
        <MoreView id={ROUTES.MORE}/>
      </Epic>
    }
    </>
  );
};

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    setDataUserVK: (data) => {
      dispatch(setDataUserVK(data));
    },
    getDataUser: (id) => {
      dispatch(getDataUser(id));
    }
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(App);

