import React, {Fragment} from 'react';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import './Intro.css';
import Spinner from "@vkontakte/vkui/dist/components/Spinner/Spinner";

const Intro = ({id, fetchedUser}) => (
  <Panel id={id} centered={true}>
    <PanelHeader>SSE</PanelHeader>
    {fetchedUser &&
    <Fragment>
      <Group>
        <Div className='User'>
          {fetchedUser.photo_200 ? <Avatar size={100} src={fetchedUser.photo_200}/> : null}
          <h2>Привет, {fetchedUser.first_name}</h2>
          <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa deleniti ducimus fuga temporibus tenetur
            voluptatibus!</h4>
        </Div>
      </Group>
      <Div>
        <Spinner size="large" style={{marginTop: 20}}/>
      </Div>
    </Fragment>
    }
  </Panel>
);

export default Intro;
