import React from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";

const Recommendations = ({id, route, go}) => (
  <Panel id={id}>
    <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>Рекомендации</PanelHeader>
  </Panel>
);

export default Recommendations;