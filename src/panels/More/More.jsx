import React from 'react';
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import Group from "@vkontakte/vkui/dist/components/Group/Group";
import Cell from "@vkontakte/vkui/dist/components/Cell/Cell";
import List from "@vkontakte/vkui/dist/components/List/List";
import Icon28StatisticsOutline from '@vkontakte/icons/dist/28/statistics_outline';
import Icon28Settings from '@vkontakte/icons/dist/28/settings';
import Icon28HelpOutline from '@vkontakte/icons/dist/28/help_outline';
import Icon28Notifications from '@vkontakte/icons/dist/28/notifications';
import Icon28GraphOutline from '@vkontakte/icons/dist/28/graph_outline';

import Header from "@vkontakte/vkui/dist/components/Header/Header";
import PanelHeaderContent from "@vkontakte/vkui/dist/components/PanelHeaderContent/PanelHeaderContent";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import {connect} from "react-redux";


const More = ({auth, id, routes, go}) => (
  <Panel id={id}>
    <PanelHeader>
      <PanelHeaderContent
        before={<Avatar size={36} src={auth.photo_100}/>}
      >
        Меню
      </PanelHeaderContent>
    </PanelHeader>
    <Group title="Menu content profile" header={<Header mode="secondary">Профиль</Header>}>
      <List>
        <Cell onClick={() => go(routes.REPORTS)} expandable before={<Icon28GraphOutline/>}>Отчеты</Cell>
        {/*<Cell onClick={() => go(routes.RECOMMENDATIONS)} expandable before={<Icon28StatisticsOutline/>}>Рекомендации</Cell>*/}
      </List>
    </Group>
    <Group title="Menu content app">
      {/*<Cell onClick={() => go(routes.NOTIFICATIONS)} expandable before={<Icon28Notifications/>}>Уведомления</Cell>*/}
      <Cell onClick={() => go(routes.SETTINGS)} expandable before={<Icon28Settings/>}>Настройки</Cell>
      <Cell onClick={() => go(routes.FAQ)} expandable before={<Icon28HelpOutline/>}>FAQ</Cell>
    </Group>
  </Panel>
);

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(More);