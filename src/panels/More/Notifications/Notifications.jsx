import React from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";

const Notifications = ({id, route, go}) => (
  <Panel id={id}>
    <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>Уведомления</PanelHeader>
  </Panel>
);

export default Notifications;