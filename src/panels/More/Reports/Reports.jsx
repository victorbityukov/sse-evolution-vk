import React from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import useChartConfig from "../../../hooks/useChartConfig";
import {Chart} from "react-charts";

const Reports = ({id, route, go}) => {
  const {data, randomizeData} = useChartConfig({
    series: 10
  })

  const series = React.useMemo(
    () => ({
      showPoints: false
    }),
    []
  )

  const axes = React.useMemo(
    () => [
      {primary: true, type: 'time', position: 'bottom'},
      {type: 'linear', position: 'left'}
    ],
    []
  )
  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>Отчеты</PanelHeader>
      <Div style={{maxHeight: "50vh"}}>
        <Chart data={data} series={series} axes={axes} tooltip/>
      </Div>
    </Panel>
  )
};

export default Reports;