import React from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import {MiniInfoCell} from "@vkontakte/vkui";
import Icon28HelpOutline from '@vkontakte/icons/dist/28/help_outline';
import Div from "@vkontakte/vkui/dist/components/Div/Div";

const Faq = ({id, route, go}) => (
  <Panel id={id}>
    <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>FAQ</PanelHeader>
    <MiniInfoCell
      multiline={true}
      before={<Icon28HelpOutline/>}
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, possimus?
    </MiniInfoCell>
    <Div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur cum laboriosam maxime mollitia odit,
      officiis!</Div>
    <MiniInfoCell
      multiline={true}
      before={<Icon28HelpOutline/>}
    >
      Lorem ipsum dolor sit amet, consectetur?
    </MiniInfoCell>
    <Div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, nisi.</Div>
    <MiniInfoCell
      multiline={true}
      before={<Icon28HelpOutline/>}
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad adipisci alias doloribus?
    </MiniInfoCell>
    <Div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium assumenda culpa officia repudiandae
      temporibus.</Div>
  </Panel>
);

export default Faq;