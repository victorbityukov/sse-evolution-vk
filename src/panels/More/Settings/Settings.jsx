import React, {Component} from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Select from "@vkontakte/vkui/dist/components/Select/Select";
import FormLayout from "@vkontakte/vkui/dist/components/FormLayout/FormLayout";
import {updateDataUser} from "../../../redux/auth-reducer";
import {connect} from "react-redux";
import Input from "@vkontakte/vkui/dist/components/Input/Input";
import FormLayoutGroup from "@vkontakte/vkui/dist/components/FormLayoutGroup/FormLayoutGroup";

const PRIVACY = {
  PUBLIC: 'public',
  PRIVATE: 'private'
};

const SEX = {
  MALE: 'male',
  FEMALE: 'female'
};

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      privacy: this.props.auth.info.privacy ? this.props.auth.info.privacy : PRIVACY.PUBLIC,
      sex: this.props.auth.info.sex ? this.props.auth.info.sex : ((this.props.auth.sex === 1 || this.props.auth.sex === 2) ? this.props.auth.info.sex : undefined),
      height: this.props.auth.info.height ? this.props.auth.info.height : undefined,
      birthday: this.props.auth.info.birthday ? this.props.auth.info.birthday : undefined
    }
  }

  componentWillUnmount() {
    this.props.updateDataUser(this.props.auth.id, this.state)
  }

  render() {
    let {id, route, go} = this.props;

    return (
      <Panel id={id}>
        <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>Настройки</PanelHeader>
        <FormLayout>
          <Select
            defaultValue={this.state.privacy}
            onChange={(e) => this.setState({privacy: e.target.value})}
            top="Конфиденциальность профиля"
          >
            <option
              value={PRIVACY.PUBLIC}
            >
              Для всех
            </option>
            <option
              value={PRIVACY.PRIVATE}
            >
              Закрытый
            </option>
          </Select>
          <FormLayoutGroup top="Дата рождения">
            <Input value={this.state.birthday} onChange={e => this.setState({birthday: e.target.value})}
                   type="date"/>
          </FormLayoutGroup>
          <Select defaultValue={this.state.sex}
                  onChange={e => this.setState({sex: e.target.value})}
                  top="Пол" placeholder="Выберите пол">
            <option
              value={SEX.MALE}
            >
              Мужской
            </option>
            <option
              value={SEX.FEMALE}
            >
              Женский
            </option>
          </Select>
          <FormLayoutGroup top="Рост (см)">
            <Input min="0" max="300" value={this.state.height} onChange={e => this.setState({height: e.target.value})}
                   type="number"/>
          </FormLayoutGroup>
        </FormLayout>
      </Panel>
    );
  }
}

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    updateDataUser: (id, settings) => dispatch(updateDataUser(id, settings))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);