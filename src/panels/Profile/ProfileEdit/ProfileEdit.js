import React, {Component} from 'react';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import PanelHeaderContent from "@vkontakte/vkui/dist/components/PanelHeaderContent/PanelHeaderContent";
import FormLayout from "@vkontakte/vkui/dist/components/FormLayout/FormLayout";
import Input from "@vkontakte/vkui/dist/components/Input/Input";
import FormLayoutGroup from "@vkontakte/vkui/dist/components/FormLayoutGroup/FormLayoutGroup";
import {connect} from "react-redux";
import {setNewCharacteristics} from "../../../redux/auth-reducer";
import Group from "@vkontakte/vkui/dist/components/Group/Group";

class ProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      strength: !!this.props.auth.characteristics ? this.props.auth.characteristics.strength : undefined,
      strength_video: !!this.props.auth.characteristics ? this.props.auth.characteristics.strength_video : undefined,
      speed_video: !!this.props.auth.characteristics ? this.props.auth.characteristics.speed_video : undefined,
      speed: !!this.props.auth.characteristics ? this.props.auth.characteristics.speed : undefined,
      endurance: !!this.props.auth.characteristics ? this.props.auth.characteristics.endurance : undefined,
      weight: !!this.props.auth.characteristics ? this.props.auth.characteristics.weight : undefined,
      fat: !!this.props.auth.characteristics ? this.props.auth.characteristics.fat : undefined
    };
  }

  componentWillUnmount() {
    this.props.setNewCharacteristics(this.props.auth, this.props.auth.characteristics && this.props.auth.characteristics.id, this.state)
  }

  render() {
    return (
      <Panel id={this.props.id}>
        <PanelHeader left={<PanelHeaderBack onClick={() => this.props.go(this.props.route)}/>}>
          <PanelHeaderContent
            before={<Avatar size={36} src={this.props.auth.photo_100}/>}
          >
            Редактировать
          </PanelHeaderContent>
        </PanelHeader>
        <Div>
          <FormLayout>
            <FormLayoutGroup top="Вес (кг)">
              <Input min="0" max="1000" value={this.state.weight}
                     onChange={e => this.setState({weight: e.target.value})}
                     type="number"/>
            </FormLayoutGroup>
            <FormLayoutGroup top="Жир (%)">
              <Input min="0" max="100" value={this.state.fat} onChange={e => this.setState({fat: e.target.value})}
                     type="number"/>
            </FormLayoutGroup>
            <FormLayoutGroup top="Жим лежа (кг)">
              <Input min="0" max="1000" value={this.state.strength}
                     onChange={e => this.setState({strength: e.target.value})}
                     type="number"/>
              <Group description={"Обязательно при жиме больше 150кг"}>
                <Input placeholder="Ссылка на видео YouTube" value={this.state.strength_video}
                       onChange={e => this.setState({strength_video: e.target.value})}
                       type="url"/>
              </Group>
            </FormLayoutGroup>
            <FormLayoutGroup top="Бег на 100м (сек)">
              <Input min="0" max="100" step="0.01" value={this.state.speed}
                     onChange={e => this.setState({speed: e.target.value})}
                     type="number"/>
              <Group description={"Обязательно при времени меньше 15.71сек"}>
                <Input placeholder="Ссылка на видео YouTube" value={this.state.speed_video}
                       onChange={e => this.setState({speed_video: e.target.value})}
                       type="url"/>
              </Group>
            </FormLayoutGroup>
            <FormLayoutGroup top="Бег на 10 км (сек)">
              <Input min="0" max="100" value={this.state.endurance}
                     onChange={e => this.setState({endurance: e.target.value})}
                     type="number"/>
            </FormLayoutGroup>
          </FormLayout>
        </Div>
      </Panel>
    )
  }
}

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    setNewCharacteristics: (auth, id_char_line, characteristics) => dispatch(setNewCharacteristics(auth, id_char_line, characteristics))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEdit);
