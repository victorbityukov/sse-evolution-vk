import React, {useEffect} from 'react';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Icon28EditOutline from '@vkontakte/icons/dist/28/edit_outline';
import PanelHeaderButton from "@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton";
import InfoRow from "@vkontakte/vkui/dist/components/InfoRow/InfoRow";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import styles from './Profile.module.css'
import PanelHeaderContent from "@vkontakte/vkui/dist/components/PanelHeaderContent/PanelHeaderContent";
import {getDataUser} from "../../redux/auth-reducer";
import {connect} from "react-redux";
import ProgressBar from "../../components/common/ProgressBar/ProgressBar";
import ScreenSpinner from "@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner";
import {LANG_RU} from "../../config/ru";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import Icon20EditCircleFillBlue from '@vkontakte/icons/dist/20/edit_circle_fill_blue';
import Icon20VideoCameraCircleFillRed from '@vkontakte/icons/dist/20/video_camera_circle_fill_red';

const CURRENT_STATUS = {
  'strength': 'Силовик',
  'speed': 'Спринтер',
  'endurance': 'Стайер'
};

function getNickByProgress(characters) {
  let maxValue = Math.max(characters[1].value, characters[2].value, characters[3].value);
  for (let i = 0; i < characters.length; i++) {
    if (characters[i].value === maxValue) {
      if (maxValue > 0) {
        return CURRENT_STATUS[characters[i].title];
      }
      return '...';
    }
  }
  return "...";
}

const Profile = (
  {
    auth,
    id,
    go,
    goStory,
    stories,
    routes,
    modal,
    getDataUser,
    setPopout,
    setActiveModal,
    setLinkVideo
  }) => {

  let characters = [];
  if (!!auth.characteristics) {
    characters = [
      {
        title: 'sse',
        value: auth.characteristics.sse
      },
      {
        title: 'strength',
        value: auth.characteristics.strength === 0 ? auth.characteristics.strength : Math.ceil(auth.characteristics.strength / auth.absolute.strength * 100)
      },
      {
        title: 'speed',
        value: auth.characteristics.speed === 0 ? auth.characteristics.speed : Math.ceil(auth.absolute.speed / auth.characteristics.speed * 100)
      },
      {
        title: 'endurance',
        value: auth.characteristics.endurance === 0 ? auth.characteristics.endurance : Math.ceil(auth.absolute.endurance / auth.characteristics.endurance * 100)
      },
    ];
  } else {
    characters = [
      {
        title: 'sse',
        value: 0
      },
      {
        title: 'strength',
        value: 0
      },
      {
        title: 'speed',
        value: 0
      },
      {
        title: 'endurance',
        value: 0
      },
    ];
  }

  useEffect(() => {
    if (!auth.isReadyDB) {
      setPopout(<ScreenSpinner/>)
    } else {
      setPopout(null)
    }
  }, [auth.isReadyDB]);

  const openModalYoutube = (link) => {
    setLinkVideo(link);
    setActiveModal(modal);
  };

  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderButton onClick={() => {
        go(routes.PROFILE_EDIT)
      }}><Icon28EditOutline/></PanelHeaderButton>}>
        <PanelHeaderContent>
          SSE Профиль
        </PanelHeaderContent>
      </PanelHeader>
      <Div className={styles['main-container']}>
        <Div className={styles['head-container']}>
          <Div style={{textAlign: "left"}}>
          <span
            className={styles['title-char']}>Цель:</span><br/>
            <Button mode="tertiary" style={{padding: "0"}} after={<Icon20EditCircleFillBlue/>}
                    onClick={() => go(routes.POINT)}>{(!!auth.characteristics ? LANG_RU[auth.characteristics.point] : 'Нет данных')}</Button>
          </Div>
          <Div className={styles['container-center']}>
            <Avatar src={auth.isReadyVK ? auth.photo_100 : undefined} size={100}/>
            <h3>{getNickByProgress(characters)}</h3>
          </Div>
          <Div style={{textAlign: "right"}}>
          <span
            className={styles['title-char']}>Масса (кг):</span><br/><span> {(!!auth.characteristics ? auth.characteristics.weight === 0 ? 'Нет данных' : auth.characteristics.weight : 'Нет данных')}{/*<ArrowUp>+2</ArrowUp>*/}</span><br/>
            <span
              className={styles['title-char']}>Жир (%):</span><br/><span> {(!!auth.characteristics ? auth.characteristics.fat === 0 ? 'Нет данных' : auth.characteristics.fat : 'Нет данных')}{/*<ArrowDown>+1</ArrowDown>*/}</span>
          </Div>
        </Div>
        <Div className={styles['progress-container']}>
          {characters.map((character, i) => (
            <InfoRow key={i} header={`${LANG_RU[character.title]}`}>
              {character.value !== 0 ? <ProgressBar completed={character.value}/> :
                <span>{character.title === "sse" ? "Не хватает данных" : "Нет данных"}</span>}
              {
                (auth.characteristics &&
                  Object.keys(auth.characteristics).indexOf(`${character.title}_video`) !== -1) &&
                !!auth.characteristics[`${character.title}_video`] &&
                <Button style={{margin: "2px", width: "100%"}} mode="second" after={<Icon20VideoCameraCircleFillRed/>}
                        onClick={() => openModalYoutube(auth.characteristics[`${character.title}_video`])}> Посмотреть
                  видео</Button>
              }
            </InfoRow>
          ))}
        </Div>
        <Div><Button onClick={() => goStory(stories.JOURNAL)} size="xl" mode="secondary">Тренировки</Button></Div>
        <Div><Button onClick={() => go(routes.FOOD)} size="xl" mode="secondary">Питание</Button></Div>
        <Div><Button onClick={() => go(routes.RECORDS)} size="xl" mode="secondary">Мои рекорды</Button></Div>
      </Div>
    </Panel>
  );
};

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getDataUser: (id) => {
      dispatch(getDataUser(id))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
