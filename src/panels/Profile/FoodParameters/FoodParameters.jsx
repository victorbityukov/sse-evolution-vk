import React, {Component} from 'react'
import {connect} from "react-redux";
import moment from "moment";
import {setNewCharacteristics, updateDataUser} from "../../../redux/auth-reducer";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import FormLayout from "@vkontakte/vkui/dist/components/FormLayout/FormLayout";
import FormLayoutGroup from "@vkontakte/vkui/dist/components/FormLayoutGroup/FormLayoutGroup";
import Input from "@vkontakte/vkui/dist/components/Input/Input";
import Select from "@vkontakte/vkui/dist/components/Select/Select";

const SEX = {
  MALE: 'male',
  FEMALE: 'female'
};

class FoodParameters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weight: !!this.props.auth.characteristics ? this.props.auth.characteristics.weight : undefined,
      sex: this.props.auth.info.sex ? this.props.auth.info.sex : ((this.props.auth.sex === 1 || this.props.auth.sex === 2) ? this.props.auth.info.sex : undefined),
      height: this.props.auth.info.height ? this.props.auth.info.height : undefined,
      formula: this.props.auth.info.height ? this.props.auth.info.height : undefined,
      birthday: this.props.auth.info.birthday ? this.props.auth.info.birthday : undefined,
      energy_consumption: !!this.props.auth.characteristics && !!this.props.auth.characteristics.energy_consumption ? this.props.auth.characteristics.energy_consumption : "1"
    };
    this.base = 0;
    this.age_old = moment().diff(this.props.auth.info.birthday, 'years');
  }

  componentWillUnmount() {
    this.props.setNewCharacteristics(this.props.auth, this.props.auth.characteristics && this.props.auth.characteristics.id, this.state)
    this.props.updateDataUser(this.props.auth.id, this.state)
  }

  render() {
    return (
      <Panel id={this.props.id}>
        <PanelHeader left={<PanelHeaderBack onClick={() => this.props.go(this.props.route)}/>}>Параметры</PanelHeader>
        <FormLayout>
          <FormLayoutGroup top="Дата рождения">
            <Input value={this.state.birthday} onChange={e => this.setState({birthday: e.target.value})}
                   type="date"/>
          </FormLayoutGroup>
          <Select defaultValue={this.state.sex}
                  onChange={e => this.setState({sex: e.target.value})}
                  top="Пол" placeholder="Выберите пол">
            <option
              value={SEX.MALE}
            >
              Мужской
            </option>
            <option
              value={SEX.FEMALE}
            >
              Женский
            </option>
          </Select>
          <FormLayoutGroup top="Рост (см)">
            <Input min="0" max="300" value={this.state.height} onChange={e => this.setState({height: e.target.value})}
                   type="number"/>
          </FormLayoutGroup>
          <FormLayoutGroup top="Вес (кг)">
            <Input min="0" max="1000" value={this.state.weight}
                   onChange={e => this.setState({weight: e.target.value})}
                   type="number"/>
          </FormLayoutGroup>
          <Select top="Уровень расхода энергии"
                  onChange={e => this.setState({energy_consumption: e.target.value})}
                  defaultValue={this.state.energy_consumption}>
            <option value="1">Стартер (1ч/нед)</option>
            <option value="3">Новичок (3ч/нед)</option>
            <option value="5">Продвинутый (5ч/нед)</option>
            <option value="8">Увлеченный (8ч/нед)</option>
            <option value="10">Полупрофессионал (10ч/нед)</option>
            <option value="12">Профессионал (12ч/нед)</option>
          </Select>
        </FormLayout>
      </Panel>
    )
  }
}


let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    updateDataUser: (id, settings) => dispatch(updateDataUser(id, settings)),
    setNewCharacteristics: (auth, id_char_line, characteristics) => dispatch(setNewCharacteristics(auth, id_char_line, characteristics))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FoodParameters);