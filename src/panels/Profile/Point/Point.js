import React, {Component} from 'react';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import PanelHeaderContent from "@vkontakte/vkui/dist/components/PanelHeaderContent/PanelHeaderContent";
import FormLayout from "@vkontakte/vkui/dist/components/FormLayout/FormLayout";
import {connect} from "react-redux";
import {setNewCharacteristics, updateDataUser} from "../../../redux/auth-reducer";
import Radio from "@vkontakte/vkui/dist/components/Radio/Radio";
import {LANG_RU} from "../../../config/ru";
import CardGrid from "@vkontakte/vkui/dist/components/CardGrid/CardGrid";
import Card from "@vkontakte/vkui/dist/components/Card/Card";
import Caption from "@vkontakte/vkui/dist/components/Typography/Caption/Caption";
import Select from "@vkontakte/vkui/dist/components/Select/Select";

const SSE = {
  SSE: 'sse',
  STRENGTH: 'strength',
  SPEED: 'speed',
  ENDURANCE: 'endurance',
};

class Point extends Component {
  constructor(props) {
    super(props);
    this.state = {
      point: !!this.props.auth.characteristics ? this.props.auth.characteristics.point : SSE.SSE,
      energy_consumption: this.props.auth.characteristics ? this.props.auth.characteristics.energy_consumption : undefined
    };
  }

  componentWillUnmount() {
    this.props.setNewCharacteristics(this.props.auth, this.props.auth.characteristics && this.props.auth.characteristics.id, this.state)
  }

  render() {
    return (
      <Panel id={this.props.id}>
        <PanelHeader left={<PanelHeaderBack onClick={() => this.props.go(this.props.route)}/>}>
          <PanelHeaderContent
            before={<Avatar size={36} src={this.props.auth.photo_100}/>}
          >
            Цель
          </PanelHeaderContent>
        </PanelHeader>
        <Div>
          <FormLayout>
            {Object.keys(SSE).map((point, i) => (
              <Radio key={i}
                     defaultChecked={SSE[point] === (this.props.auth.characteristics ? this.props.auth.characteristics.point : SSE.SSE)}
                     name="point"
                     value={SSE[point]}
                     onChange={e => this.setState({point: e.target.value})}>
                <CardGrid>
                  <Caption level="1" weight="regular">{LANG_RU[SSE[point]]}</Caption>
                  {this.state.point === SSE[point] ? <Card size="l"
                                                                            style={{background: "url(https://sse.readman.pro/assets/images/fire.jpg)", transition: "all 300ms"}}>
                    <div style={{
                      height: 90,
                      color: "#ffffff",
                      padding: "5px",
                      fontSize: "12px",
                      textShadow: "0px 0px 5px #000000"
                    }}>
                      Lorem ipsum dolor sit amet, consectetur adipisicing
                      elit. Inventore, nisi ullam? Dolorem magni maxime nostrum?
                    </div>
                  </Card> : "" }
                </CardGrid>
              </Radio>
            ))}
            <Select top="Уровень расхода энергии"
                    onChange={e => this.setState({energy_consumption: e.target.value})}
                    value={this.state.energy_consumption}>
              <option value="1">Стартер (1ч/нед)</option>
              <option value="3">Новичок (3ч/нед)</option>
              <option value="5">Продвинутый (5ч/нед)</option>
              <option value="8">Увлеченный (8ч/нед)</option>
              <option value="10">Полупрофессионал (10ч/нед)</option>
              <option value="12">Профессионал (12ч/нед)</option>
            </Select>
          </FormLayout>
        </Div>
      </Panel>
    )
  }
}

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    updateDataUser: (id, settings) => dispatch(updateDataUser(id, settings)),
    setNewCharacteristics: (auth, id_char_line, characteristics) => dispatch(setNewCharacteristics(auth, id_char_line, characteristics))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Point);
