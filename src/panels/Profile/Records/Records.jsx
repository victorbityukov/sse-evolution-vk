import React, {useState, useEffect} from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import Group from "@vkontakte/vkui/dist/components/Group/Group";
import ProgressBar from "../../../components/common/ProgressBar/ProgressBar";
import {getRecordsUser} from "../../../redux/auth-reducer";
import {connect} from "react-redux";
import ScreenSpinner from "@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner";
import {LANG_RU} from "../../../config/ru";
import SimpleCell from "@vkontakte/vkui/dist/components/SimpleCell/SimpleCell";
import Header from "@vkontakte/vkui/dist/components/Header/Header";

const Records = ({auth, id, route, go, setPopout, getRecordsUser}) => {
  const [characters, setCharacters] = useState([]);
  useEffect(() => {
    setPopout(<ScreenSpinner/>);
    getRecordsUser(auth.info.id)
  }, []);

  useEffect(() => {
    if (auth.records) {
      setCharacters([
        {
          title: 'sse',
          percent: ((auth.records.strength.value === 0) || (auth.records.speed.value === 0) || (auth.records.endurance.value === 0)) ? 0 : Math.ceil((auth.records.strength.value / auth.absolute.strength + auth.absolute.speed / auth.records.speed.value + auth.absolute.endurance / auth.records.endurance.value) / 3 * 100),
          value: ((auth.records.strength.value === 0) || (auth.records.speed.value === 0) || (auth.records.endurance.value === 0)) ? 0 : Math.ceil((auth.records.strength.value / auth.absolute.strength + auth.absolute.speed / auth.records.speed.value + auth.absolute.endurance / auth.records.endurance.value) / 3 * 100),
          metric: '%'
        },
        {
          title: 'strength',
          percent: auth.records.strength.value === 0 ? auth.records.strength.value : Math.ceil(auth.records.strength.value / auth.absolute.strength * 100),
          value: auth.records.strength.value,
          date: auth.records.strength.date,
          metric: 'кг'
        },
        {
          title: 'speed',
          percent: auth.records.speed.value === 0 ? auth.records.speed.value : Math.ceil(auth.absolute.speed / auth.records.speed.value * 100),
          value: auth.records.speed.value,
          date: auth.records.speed.date,
          metric: 'сек'
        },
        {
          title: 'endurance',
          percent: auth.records.endurance.value === 0 ? auth.records.endurance.value : Math.ceil(auth.absolute.endurance / auth.records.endurance.value * 100),
          value: auth.records.endurance.value,
          date: auth.records.endurance.date,
          metric: 'сек'
        },
      ]);
      setPopout(null);
    }
  }, [auth.records]);

  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>Мои рекорды</PanelHeader>
      <Group>
        {characters.map((character, i) => (
          <Div key={i}>
            <Header mode="secondary" aside={character.date} indicator={`${character.value}${character.metric}`}>
              {`${LANG_RU[character.title]}`}
            </Header>
            <SimpleCell>
              {character.percent !== 0 ? <ProgressBar completed={character.percent}/> :
                <span>{character.title === "sse" ? "Не хватает данных" : "Нет данных"}</span>}
            </SimpleCell>
          </Div>
        ))}
      </Group>
      <Div>
        Рекомендация от цели
        Ты достаточно быстр! Работай над силой!
      </Div>
    </Panel>
  );
};

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getRecordsUser: (id) => {
      dispatch(getRecordsUser(id))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Records);
