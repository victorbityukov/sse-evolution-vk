import React, {useState, useEffect} from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import Tabs from "@vkontakte/vkui/dist/components/Tabs/Tabs";
import TabsItem from "@vkontakte/vkui/dist/components/TabsItem/TabsItem";
import Separator from "@vkontakte/vkui/dist/components/Separator/Separator";
import {LANG_RU} from "../../../config/ru";
import moment from "moment";
import {connect} from "react-redux";
import Text from "@vkontakte/vkui/dist/components/Typography/Text/Text";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import {setNewCharacteristics} from "../../../redux/auth-reducer";

const TABS = {
  RECOMMENDATION: 'recommendation',
  FAQ: 'faq'
};

const KOEF_POINT = {
  "strength": 1.04,
  "speed": 0.47,
  "endurance": 0.34,
  "sse": 0.47 + 0.035
};

const Food = ({id, auth, routes, go, setNewCharacteristics}) => {
  useEffect(() => {
    if (!auth.characteristics) {
      setNewCharacteristics(auth, auth.characteristics, {})
    }
  }, []);

  const [weight, setWeight] = useState(!!auth.characteristics ? auth.characteristics.weight : undefined);
  const [sex, setSex] = useState(auth.info.sex ? auth.info.sex : ((auth.sex === 1 || auth.sex === 2) ? auth.info.sex : undefined));
  const [height, setHeight] = useState(auth.info.height ? auth.info.height : undefined);
  const [energy_consumption, setEnergy_consumption] = useState(auth.characteristics ? auth.characteristics.energy_consumption : undefined);
  const [age_old, setAge_old] = useState(moment().diff(auth.info.birthday, 'years'));
  const [tab, setTab] = useState(TABS.RECOMMENDATION);
  const [base, setBase] = useState(sex === 'male' ? Math.round((10 * weight) + (6.25 * height) - (5 * age_old) + 5) :
    Math.floor(10 * weight + 6.25 * height - 5 * age_old + 5) - 161);
  const [idealWeight, setIdealWeight] = useState(auth.info.height && Math.floor(height * KOEF_POINT[auth.characteristics.point]));
  const [recommendation, setRecommendation] = useState(null);

  const calculateRecommendation = () => {
    if (!auth.characteristics.weight) {
      return 'Не хватает данных';
    }
    if (auth.characteristics.weight === idealWeight) {
      return 'Ваш вес идеален';
    } else {
      if (Math.abs(auth.characteristics.weight - idealWeight) < 5) {
        return 'Ваш вес близок к идеальному';
      }
    }

    if (!auth.characteristicsWeekAgo) {
      if (auth.characteristics.weight > idealWeight) {
        return 'Уменьшите количество калорий на 10%';
      } else {
        return 'Увеличьте количество калорий на 10%';
      }
    } else {
      if ((Math.abs(auth.characteristics.weight - idealWeight)) > (Math.abs(auth.characteristicsWeekAgo.weight - idealWeight))) {
        return 'Продолжайте в том же духе'
      } else {
        if (auth.characteristics.weight > idealWeight) {
          return 'Уменьшите количество калорий ещё на 5%';
        } else {
          return 'Увеличьте количество калорий ещё на 5%';
        }
      }
    }
  };


  useEffect(() => {
    if (auth.isReadyDB && auth.characteristics) {
      setWeight(!!auth.characteristics ? auth.characteristics.weight : undefined);
      setSex(auth.info.sex ? auth.info.sex : ((auth.sex === 1 || auth.sex === 2) ? auth.info.sex : undefined));
      setHeight(auth.info.height ? auth.info.height : undefined);
      setEnergy_consumption(auth.characteristics ? auth.characteristics.energy_consumption : undefined);
      setAge_old(moment().diff(auth.info.birthday, 'years'));
      setTab(TABS.RECOMMENDATION);
      setIdealWeight(auth.info.height && Math.floor(auth.info.height * KOEF_POINT[auth.characteristics.point]));
      setRecommendation(calculateRecommendation());
    }
  }, [auth.isReadyDB]);

  useEffect(() => {
    setBase(sex === 'male' ? Math.round((10 * weight) + (6.25 * height) - (5 * age_old) + 5) :
      Math.floor(10 * weight + 6.25 * height - 5 * age_old + 5) - 161);
  }, [weight, height, age_old]);

  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderBack onClick={() => go(routes.PROFILE)}/>}>Питание</PanelHeader>
      <Tabs>
        <TabsItem
          onClick={() => setTab(TABS.RECOMMENDATION)}
          selected={tab === TABS.RECOMMENDATION}
        >
          Рекомендации
        </TabsItem>
        <TabsItem
          onClick={() => setTab(TABS.FAQ)}
          selected={tab === TABS.FAQ}
        >
          FAQ
        </TabsItem>
      </Tabs>
      <Separator/>
      {tab === TABS.RECOMMENDATION && auth.characteristics &&
      <Div>
        <Text weight="semibold">Базовый обмен:</Text>
        <h2
          style={{textAlign: "center"}}>
          {base ? `${base}ккал` : 'Не хватает данных'}
        </h2>
        <Text weight="semibold">Для поддержания веса необходимо:</Text>
        <h2 style={{textAlign: "center"}}>
          {(weight && energy_consumption) ? `${Math.floor(((10 * energy_consumption * weight) / 7) + base + (8 * weight))}ккал` : 'Не хватает данных'}
        </h2>
        <Text weight="semibold">Ваш вес:</Text>
        <h2 style={{textAlign: "center"}}>
          {auth.characteristics.weight ? `${auth.characteristics.weight}кг` : 'Нет данных'}
        </h2>
        <Text weight="semibold">Идеальный вес ({LANG_RU[auth.characteristics.point]}):</Text>
        <h2 style={{textAlign: "center"}}>
          {idealWeight ? `${idealWeight}кг` : 'Не хватает данных'}
        </h2>
        <Text weight="semibold">Рекомендации:</Text>
        <Text weight="medium">
          {recommendation}
        </Text>
        <br/>
        <Button size={"xl"} onClick={() => go(routes.FOOD_PARAMETERS)}>Параметры для расчета</Button>
      </Div>
      }
      {tab === TABS.FAQ && <Div>Общие рекомендации (ответы на вопросы)</Div>}
    </Panel>
  )
};

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    setNewCharacteristics: (auth, id_char_line, characteristics) => dispatch(setNewCharacteristics(auth, id_char_line, characteristics))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Food);