import React from 'react'
import {IconDefault, IconEndurance, IconSpeed, IconStrength} from "../../../components/common/Icons/Icons";

import styles from './EventBlock.module.css'

const EventBlock = ({data, style}) => {
  if (data) {
    return (
      <div className={`${styles["event-container"]} ${styles[style]}`}>
        <div
          className={styles['icon']}>{data.type === 'strength' ? IconStrength : data.type === 'speed' ? IconSpeed : data.type === 'endurance' ? IconEndurance : IconDefault}</div>
        <div className={styles['content-container']}>
          <div className={styles["title"]}>{data.title}</div>
          <div className={styles["time"]}>{!!data.range_time ? data.range_time.substr(0, 5) : "--:--"}</div>
        </div>
      </div>
    )
  }
  return '';
};

export default EventBlock;