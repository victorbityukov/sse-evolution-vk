import React, {useState, useEffect} from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import PanelHeaderBack from "@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import FormLayoutGroup from "@vkontakte/vkui/dist/components/FormLayoutGroup/FormLayoutGroup";
import Input from "@vkontakte/vkui/dist/components/Input/Input";
import FormLayout from "@vkontakte/vkui/dist/components/FormLayout/FormLayout";
import moment from "moment";
import {LANG_RU} from "../../../config/ru";
import Select from "@vkontakte/vkui/dist/components/Select/Select";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import {connect} from "react-redux";
import {addEvent, changeEvent, deleteEvent} from "../../../redux/events-reducer";
import Tabs from "@vkontakte/vkui/dist/components/Tabs/Tabs";
import TabsItem from "@vkontakte/vkui/dist/components/TabsItem/TabsItem";
import {Textarea} from "@vkontakte/vkui";

const EVENTS_TYPES = {
  STRENGTH: 'strength',
  SPEED: 'speed',
  ENDURANCE: 'endurance',
};

const TABS = {
  TRAINING: 'training',
  COMMENTS: 'comments'
};

const EditEvent = ({id, uid, route, go, addEvent, changeEvent, deleteEvent, event}) => {
  const [action, setAction] = useState(id === 'add-event' ? 'new' : 'edit');
  const [tab, setTab] = useState(TABS.TRAINING);
  const [date, setDate] = useState(event ? event.date_event : moment().format('YYYY-MM-DD'));
  const [time, setTime] = useState(event ? event.time_event : moment().format('hh:mm'));
  const [title, setTitle] = useState(event ? event.title : '');
  const [type, setType] = useState(event ? event.type : EVENTS_TYPES.OTHER);
  const [value, setValue] = useState(event ? JSON.parse(event.value_event) : {});



  const [averageHeartRate, setAverageHeartRate] = useState(value.hasOwnProperty('averageHeartRate') ? value.averageHeartRate : '');
  const [maxHeartRate, setMaxHeartRate] = useState(value.hasOwnProperty('maxHeartRate') ? value.maxHeartRate : '');
  const [kkal, setKkal] = useState(value.hasOwnProperty('kkal') ? value.kkal : '');
  const [runningVolume, setRunningVolume] = useState(value.hasOwnProperty('runningVolume') ? value.runningVolume : '');
  const [averagePace, setAveragePace] = useState(value.hasOwnProperty('averagePace') ? value.averagePace : '');
  const [comments, setComments] = useState(event ? event.comments : '');
  const [rangeTime, setRangeTime] = useState(event ? event.range_time : '');
  const [factRangeTime, setFactRangeTime] = useState(event ? event.fact_range_time : '');

  useEffect(() => {
    setValue({
      averageHeartRate,
      maxHeartRate,
      kkal,
      runningVolume,
      averagePace,
      value
    });
  }, [averageHeartRate, maxHeartRate, kkal]);

  const newEvent = async () => {
    let result = {
      id: uid,
      value_event: JSON.stringify(value),
      date_event: date,
      time_event: time,
      range_time: rangeTime,
      fact_range_time: factRangeTime,
      comments,
      title,
      type
    };
    await addEvent(result);
    go(route);
  };
  const updateEvent = async () => {
    let result = {
      id: uid,
      id_event: event.id,
      value_event: JSON.stringify(value),
      date_event: date,
      time_event: time,
      range_time: rangeTime,
      fact_range_time: factRangeTime,
      comments,
      title,
      type
    };
    await changeEvent(result);
    go(route);
  };

  const destroyEvent = async () => {
    let result = {
      id: uid,
      id_event: event.id,
    };
    await deleteEvent(result);
    go(route);
  };


  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderBack onClick={() => go(route)}/>}>Событие</PanelHeader>
      <Tabs>
        <TabsItem
          onClick={() => setTab(TABS.TRAINING)}
          selected={tab === TABS.TRAINING}
        >
          Тренировка
        </TabsItem>
        <TabsItem
          onClick={() => setTab(TABS.COMMENTS)}
          selected={tab === TABS.COMMENTS}
        >
          Комментарии
        </TabsItem>
      </Tabs>
      <Div>
        {tab === TABS.TRAINING ?
        <FormLayout>
          <FormLayoutGroup top="Название">
            <Input value={title}
                   onChange={e => setTitle(e.target.value)}
                   type="text"/>
          </FormLayoutGroup>
          <Select
            defaultValue={type}
            onChange={(e) => setType(e.target.value)}
            top="Тип тренировки"
          >
            <option
              value={""}
            >
              Выбрать тип тренировки
            </option>
            {Object.keys(EVENTS_TYPES).map((key, i) => (
              <option
                key={i}
                value={EVENTS_TYPES[key]}
              >
                {LANG_RU[EVENTS_TYPES[key]]}
              </option>
            ))}
          </Select>
          <FormLayoutGroup top="Дата события">
            <Input value={date}
                   onChange={e => setDate(e.target.value)}
                   type="date"/>
            <Input value={time}
                   onChange={e => setTime(e.target.value)}
                   type="time"/>
          </FormLayoutGroup>
          <FormLayoutGroup top="Продолжительность тренировки (план):">
            <Input value={rangeTime}
                   onChange={e => setRangeTime(e.target.value)}
                   type="time"/>
          </FormLayoutGroup>
          <FormLayoutGroup top="Продолжительность тренировки (факт):">
            <Input value={factRangeTime}
                   onChange={e => setFactRangeTime(e.target.value)}
                   type="time"/>
          </FormLayoutGroup>
          {(type === "speed" || type === "endurance") && <FormLayoutGroup top="Общий беговой объем (м):">
            <Input value={runningVolume}
                   onChange={e => setRunningVolume(e.target.value)}
                   type="number"/>
          </FormLayoutGroup>}
          {(type === "speed" || type === "endurance") && <FormLayoutGroup top="Средний темп на 1км:">
            <Input value={averagePace}
                   onChange={e => setAveragePace(e.target.value)}
                   type="number"/>
          </FormLayoutGroup>}
          <FormLayoutGroup top="Средний пульс:">
            <Input value={averageHeartRate}
                   onChange={e => setAverageHeartRate(e.target.value)}
                   type="number"/>
          </FormLayoutGroup>
          {type === "strength" && <FormLayoutGroup top="Максимальный пульс:">
            <Input value={maxHeartRate}
                   onChange={e => setMaxHeartRate(e.target.value)}
                   type="number"/>
          </FormLayoutGroup>}
          <FormLayoutGroup top="Калории:">
            <Input value={kkal}
                   onChange={e => setKkal(e.target.value)}
                   type="number"/>
          </FormLayoutGroup>
          {action === 'new' ? <Button size="xl" mode="primary" onClick={newEvent}>Добавить</Button> :
            <Button size="xl" mode="primary" onClick={updateEvent}>Сохранить</Button>}
          <Button size="xl" mode="outline" onClick={() => go(route)}>Отменить</Button>
          {action === 'edit' && <Button size="xl" mode="destructive" onClick={destroyEvent}>Удалить</Button>}
        </FormLayout> :
          <FormLayout>
            <Textarea placeholder="Комментарий к тренировке..." value={comments} onChange={e => setComments(e.target.value)}/>
            {action === 'new' ? <Button size="xl" mode="primary" onClick={newEvent}>Добавить</Button> :
              <Button size="xl" mode="primary" onClick={updateEvent}>Сохранить</Button>}
            <Button size="xl" mode="outline" onClick={() => go(route)}>Отменить</Button>
            {action === 'edit' && <Button size="xl" mode="destructive" onClick={destroyEvent}>Удалить</Button>}
          </FormLayout>}
      </Div>
    </Panel>
  );
};

let mapStateToProps = ({auth}) => {
  return {
    uid: auth.info.id
  }
};

let mapDispatchToProps = dispatch => {
  return {
    addEvent: data => dispatch(addEvent(data)),
    changeEvent: data => dispatch(changeEvent(data)),
    deleteEvent: data => dispatch(deleteEvent(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditEvent);