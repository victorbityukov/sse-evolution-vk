import React, {useRef, useEffect, useState} from 'react';
import Moment from "react-moment";
import styles from "./List.module.css";
import moment from "moment";
import EventBlock from "../EventBlock/EventBlock";

let dayWeek = '';
let currentDateFormat = moment().format('YYYY-MM-DD')

function getStyleBlock(event, currentDate) {
  if (event.date_event <= currentDate) {
    if (event.range_time === "00:00:00") {
      return 'neutral';
    } else {
      if (event.fact_range_time !== "00:00:00") {
        if (event.range_time === event.fact_range_time) {
          return 'excellent'
        } else {
          return 'good'
        }
      } else {
        return 'bad'
      }
    }
  } else {
    return 'neutral'
  }
}

const List = ({data, rowHeight, numberCurrent, visibleRows, goEvent, currentDate, setCurrentDate, routes}) => {
  const rootRef = useRef();
  const [start, setStart] = useState(0);

  function getTopHeight() {
    return rowHeight * start;
  }

  function getBottomHeight() {
    return rowHeight * (data.length - (start + visibleRows + 1));
  }

  useEffect(() => {
    let containerList = document.getElementById('container-list');
    containerList.scrollTop = numberCurrent * rowHeight;
  }, []);

  useEffect(() => {
    function onScroll(e) {
      setStart(Math.min(
        data.length - visibleRows - 1,
        Math.floor(e.target.scrollTop / rowHeight)
      ));
    }

    rootRef.current.addEventListener('scroll', onScroll);

    return () => {
      if (rootRef.current) {
        rootRef.current.removeEventListener('scroll', onScroll);
      }
    }
  }, [data.length, visibleRows, rowHeight]);

  useEffect(() => {
    if (data.length) {
      let nextDate = moment(data[start].date).format();
      let now = new Date(nextDate);
      setCurrentDate(moment(now).format());
    } else {
      setCurrentDate(moment().format());
    }
  }, [start]);

  const getDayWeek = (row) => {
    if (dayWeek !== moment(row).format("DD dd")) {
      dayWeek = moment(row).format("DD dd");
      return (
        <div>
          <div className={styles['day']}><Moment locale='ru' format="DD">{row}</Moment></div>
          <div className={styles['day-week']}><Moment locale='ru' format="dd">{row}</Moment></div>
        </div>
      )
    }
    return null;
  };

  return (
    <div id="container-list" className={styles['container-list']}
         style={{height: rowHeight * visibleRows + 1, overflow: 'auto'}}
         ref={rootRef}>
      <div style={{height: getTopHeight()}}/>
      <table className={styles['table-list']} cellSpacing="15">
        <tbody>
        {data.slice(start, start + visibleRows + 1).map((row, rowIndex) => (
          <tr
            className={styles['line-event']}
            style={{height: rowHeight}}
            key={start + rowIndex}
          >
            <td className={styles['date']}>
              {getDayWeek(row.date)}
            </td>
            {row.event ?
              <td onClick={() => goEvent(routes.EDIT_EVENT, row.event)} className={styles['event']}>
                <EventBlock data={row.event} style={getStyleBlock(row.event, currentDateFormat)}/>
              </td> :
              <td className={styles['event']}>
                <EventBlock data={row.event}/>
              </td>
            }
          </tr>
        ))}
        </tbody>
      </table>
      <div style={{height: getBottomHeight()}}/>
    </div>
  )
};


export default List;