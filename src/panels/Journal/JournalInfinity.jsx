import React, {useState, useEffect} from 'react';
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import 'react-calendar/dist/Calendar.css';
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import Moment from "react-moment";
import 'moment/locale/ru';
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import Icon36Add from '@vkontakte/icons/dist/36/add';
import {getAllDays} from "../../redux/events-reducer";
import {connect} from "react-redux";
import List from "./List/List";
import styles from './JournalInfinity.module.css';


const JournalInfinity = ({id, uid, events, numberCurrent, firstDate, lastDate, goEvent, getAllDays, go, routes}) => {
    const [currentDate, setCurrentDate] = useState(null);

    useEffect(() => {
      getAllDays(uid);
    }, []);

    return (
      <Panel id={id}>
        <PanelHeader
          // left={<PanelHeaderButton><Icon28CalendarOutline/></PanelHeaderButton>}
        >
          <div className={styles['month-year']}><Moment locale='ru' format="MMMM YYYY">{currentDate}</Moment></div>
        </PanelHeader>
        <Div id="list-journal">
          {events &&
          <List
            data={events}
            rowHeight={60}
            visibleRows={Math.round(window.innerHeight * 0.0135)}
            goEvent={goEvent}
            numberCurrent={numberCurrent}
            currentDate={currentDate}
            setCurrentDate={setCurrentDate}
            routes={routes}
          />
          }
        </Div>
        <Button className={styles["btn-add"]} onClick={() => go(routes.ADD_EVENT)}><Icon36Add/></Button>
      </Panel>
    )
  }
;


let mapStateToProps = ({auth, events}) => {
  return {
    uid: auth.info.id,
    events: events.events,
    firstDate: events.firstDate,
    lastDate: events.lastDate,
    numberCurrent: events.numberCurrent
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getAllDays: (id_user) => dispatch(getAllDays(id_user)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(JournalInfinity);