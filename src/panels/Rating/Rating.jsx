import React, {useState, useEffect} from 'react';
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import List from "@vkontakte/vkui/dist/components/List/List";
import Group from "@vkontakte/vkui/dist/components/Group/Group";
import SimpleCell from "@vkontakte/vkui/dist/components/SimpleCell/SimpleCell";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import Select from "@vkontakte/vkui/dist/components/Select/Select";
import {connect} from "react-redux";
import {getRating} from "../../redux/auth-reducer";
import FixedLayout from "@vkontakte/vkui/dist/components/FixedLayout/FixedLayout";
import Spinner from "@vkontakte/vkui/dist/components/Spinner/Spinner";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import {LANG_RU} from "../../config/ru";
import CardGrid from "@vkontakte/vkui/dist/components/CardGrid/CardGrid";
import Card from "@vkontakte/vkui/dist/components/Card/Card";
import Icon24Videocam from '@vkontakte/icons/dist/24/videocam';

const FILTERS = {
  SSE: 'sse',
  STRENGTH: 'strength',
  SPEED: 'speed',
  ENDURANCE: 'endurance'
};

const Rating = ({auth, id, getRating}) => {
  const [filter, setFilter] = useState(FILTERS.SSE);
  const [contextOpened, setContextOpened] = useState(false);
  const [rating, setRating] = useState(null);
  const [valueUser, setValueUser] = useState();

  useEffect(() => {
    getRating(auth.info.id, filter);
  }, []);

  useEffect(() => {
    getRating(auth.info.id, filter);

    switch (filter) {
      case FILTERS.SSE:
        setValueUser(auth.characteristics.sse);
        break;
      case FILTERS.STRENGTH:
        setValueUser(auth.characteristics.strength);
        break;
      case FILTERS.SPEED:
        setValueUser(auth.characteristics.speed);
        break;
      case FILTERS.ENDURANCE:
        setValueUser(auth.characteristics.endurance);
        break;

    }
  }, [filter]);

  useEffect(() => {
    if (auth.rating) {
      setRating(auth.rating);
    }
  }, [auth.rating]);

  const toggleContext = () => {
    setContextOpened(!contextOpened);
  };

  const select = (e) => {
    setFilter(e.target.value);
    requestAnimationFrame(toggleContext);
  };

  return (
    <Panel id={id}>
      <PanelHeader>
        Рейтинг
      </PanelHeader>
      <Div>
        <Select defaultValue={filter} onChange={select} top="Цель">
          <option
            value={FILTERS.SSE}
          >
            {LANG_RU[FILTERS.SSE]} (%)
          </option>
          <option
            value={FILTERS.STRENGTH}
          >
            {LANG_RU[FILTERS.STRENGTH]} (кг)
          </option>
          <option
            value={FILTERS.SPEED}
          >
            {LANG_RU[FILTERS.SPEED]} (сек)
          </option>
          <option
            value={FILTERS.ENDURANCE}
          >
            {LANG_RU[FILTERS.ENDURANCE]} (сек)
          </option>
        </Select>
      </Div>
      <Group>
        <List>
          {!!rating ?
            rating.items.map((user, i) => (
              <SimpleCell before={<>{i + 1}.&nbsp;<Avatar src={user.photo_50}/></>} key={i}
                          after={<>{user.video && <Icon24Videocam
                            style={{display: "inline-block"}}/>} {user.result}</>}>{`${user.last_name} ${user.first_name}`}</SimpleCell>
            ))
            :
            <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
              <Spinner size="large"/>
            </div>
          }
        </List>
      </Group>
      <FixedLayout vertical="bottom">
        <CardGrid>
          <Card size="l" mode="shadow">
            {!!rating && valueUser !== 0 && rating.position !== 0 ?
              <SimpleCell before={<>{rating.position}.&nbsp;<Avatar src={auth.photo_100}/></>}
                          after={`${valueUser}`}>{`${auth.last_name} ${auth.first_name}`}</SimpleCell> :
              <SimpleCell before={<Avatar src={auth.photo_100}/>}
                          after={valueUser ? 'Нет подтверждения' : 'Нет данных'}>{`${auth.last_name} ${auth.first_name}`}</SimpleCell>}
          </Card>
        </CardGrid>

      </FixedLayout>
    </Panel>
  );
};
let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getRating: (id, field) => dispatch(getRating(id, field))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Rating);