import React from 'react';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import Group from "@vkontakte/vkui/dist/components/Group/Group";
import CardGrid from "@vkontakte/vkui/dist/components/CardGrid/CardGrid";
import Card from "@vkontakte/vkui/dist/components/Card/Card";
import SimpleCell from "@vkontakte/vkui/dist/components/SimpleCell/SimpleCell";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import getLinkImg from "../../utils/temp_values";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import Text from "@vkontakte/vkui/dist/components/Typography/Text/Text";
import Icon28SlidersOutline from '@vkontakte/icons/dist/28/sliders_outline';
import PanelHeaderButton from "@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton";
import {connect} from "react-redux";

const News = ({auth, id, activeModal, setActiveModal}) => (
  <Panel id={id}>
    <PanelHeader left={<PanelHeaderButton  onClick={() => setActiveModal(activeModal)}><Icon28SlidersOutline/></PanelHeaderButton>}>Новости</PanelHeader>
    <Group separator="hide">
      <CardGrid>
        <Card size="l" mode="shadow">
          <SimpleCell
            before={<Avatar size={48}
                            src={auth.photo_100}/>}
            description="7 окт в 13:10">{`${auth.first_name} ${auth.last_name}`}</SimpleCell>
          <Div>
            <Text weight="regular" style={{marginBottom: 16}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Asperiores aspernatur distinctio esse, eveniet fuga ipsam ipsum iste necessitatibus non nostrum possimus
              quam rerum sequi velit.</Text>
          </Div>
        </Card>
        <Card size="l" mode="shadow">
          <SimpleCell
            before={<Avatar size={48}
                            src={auth.photo_100}/>}
            description="7 окт в 13:10">{`${auth.first_name} ${auth.last_name}`}</SimpleCell>
          <Div>
            <Text weight="regular" style={{marginBottom: 16}}>Lorem ipsum dolor sit amet.</Text>
          </Div>
        </Card>
      </CardGrid>
    </Group>
  </Panel>
);

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};

let mapDispatchToProps = dispatch => {
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(News);